<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class APIController extends Controller
{
	public $successStatus = 200;
    /**
     * Show the subscribed users list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subscribers(Request $request)
    {
        if(FunctionsController::headerAuthentication($request)){
			$data = Subscriber::all();
			return FunctionsController::response(true, "Subscribers list fetched successfully", $data, $this->successStatus);
		} 
		return FunctionsController::response(false, "Unauthorized", (object) [], 401);
    }
}
