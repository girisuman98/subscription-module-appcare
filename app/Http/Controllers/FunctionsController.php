<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FunctionsController extends Controller
{
	public static function headerAuthentication($request)
	{
		$header = $request->header('Authorization');
		$user = User::where("username", "service_account")->first();
        return $header == "Basic ".base64_encode($user->username.":".$user->password);
    }
	
    public static function response($status, $message, $data, $status_code)
	{
        return response()->json([
			'status' => $status,
			'message' => $message,
			'data' => $data
			], 
				$status_code
			); 
    }
}
