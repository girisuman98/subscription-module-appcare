<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
   protected $table = "wp_nls_subscribers";
}
