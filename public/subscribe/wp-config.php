<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'subscription' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'gb1UzfcWw=Oh1|g%}{ZjQ5H87 y 8[N&1M;Mh1LQGlxq|GING4Ru7%n3$~C0#qc*' );
define( 'SECURE_AUTH_KEY',  '8#W=AUQ)gmis-%sK+ovb)C/hyT%+YX%z8v3X=2y<=:x m5XoWc5r3OR|*n|HsGWX' );
define( 'LOGGED_IN_KEY',    'l35@3],usvD%W_=d~44n8adeJvA^wDIO41Xk|6F7RvLalc@-}D&;qV#5ICOy0sU{' );
define( 'NONCE_KEY',        'l)gi<2taJ1FL5SYFyj=Z5<x#Yp*vtUKaoIw2w-{ ?v?*Yec[jD07+/kO:C67wu-H' );
define( 'AUTH_SALT',        'l;m-Gl#C/5u>DG#Ps`XC2s#oJgy9e|- ?Q(P&i.*AXy,xN<%lGb*MJbfrlJYKJ*m' );
define( 'SECURE_AUTH_SALT', '!.&9j7]vD,W_scL~A,8qglC*/H(W=1/DMa`7K)V9cdrX,>&B1y%ZFv[mIJbS@[~$' );
define( 'LOGGED_IN_SALT',   'r/UN iEqi[aotla+9/YCPqn,f45pD6r{2N^Zu.R?7s2g:cc_AV+)%unnV gF sCs' );
define( 'NONCE_SALT',       '[P.qawW!8&i2N3^;LqzC)beJ7hMu4@woqp?@s7&knem2T^kpHj68i8_xhyG*iNh{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
